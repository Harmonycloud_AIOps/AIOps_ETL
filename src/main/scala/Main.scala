package com.harmonycloud

import com.harmonycloud.config.Config
import com.harmonycloud.readdata.{ReadDataFromES, ReadDataFromMysql}
import com.harmonycloud.savedata.SaveDataToHbase
import org.apache.spark.SparkConf
object Main {
  def main(args: Array[String]): Unit = {
    //传递参数0：读取数据源类型 mysql 1：IP  2:Port  3.数据库名称 4.数据库表名 5.账号  6.密码  7.关键字段列名
    //8.时间戳列名  9.时间戳上界  10.时间戳下界 11.Hbase数据库要插入的表名 12.插入数据RowKey前缀
    // 13.插入值的Hbase表中列名
    //传递参数0：读取数据源类型 es 1.esNodeIP  2.esPort 3.索引名 4.关键字段
    // 5.时间戳列名 6.筛选时间戳上界 7.筛选时间戳下界
    // 8.Hbase数据库要插入的表名 9.插入数据RowKey前缀 10.插入值的Hbase表中列名

    var  sparkConf= new SparkConf().setMaster("local").setAppName("AIOps_ETL")
    var InputError = true

    if(args.length!=14||args.length!=11) InputError = true
    else InputError = false
    if(InputError == false) {
      println("Input parameter's number error!")
      sys.exit(1)
      }
      val readData = args(0) match{
      case "mysql" =>new ReadDataFromMysql(args(1), args(2), args(3), args(4), args(5), args(6),
        args(7), args(8), args(9), args(10),sparkConf).readDataToDataFrame()
      //case "es" => new ReadDataFromES()
      case "es" => new ReadDataFromES(args(1), args(2), args(3), args(4), args(5), args(6),
        args(7),sparkConf).readDataToDataFrame()
    }
      readData.show(100)
    var saveToHbase=args(0) match {
      case "mysql" => new SaveDataToHbase(args(11), args(12), args(13), readData).SaveDataframeToHbase()
      case "es" =>new SaveDataToHbase(args(8), args(9), args(10), readData).SaveDataframeToHbase()
    }

      //readdata.config.APPName = "AIOpsETL"
    //readdata.config.masterSet = "local"

  }
}
