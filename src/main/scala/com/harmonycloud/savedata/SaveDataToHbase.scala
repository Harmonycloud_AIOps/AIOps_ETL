package com.harmonycloud.savedata

import com.harmonycloud.config.Config
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.client.Put
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.hbase.mapred.TableOutputFormat
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.mapred.JobConf
import org.apache.spark.sql.DataFrame

class SaveDataToHbase (tablename:String,HbaseRowkeyPrefix:String,HbaseInputColName:String,dataframe:DataFrame) extends Serializable {
  //1.插入表名 2.插入数据RowKey前缀 3.插入值的Hbase表中列名 4.经过处理后得到的要插入的dataframe
  val config=new Config()
  config.HbaseTableName=tablename
  config.HbaseRowkeyPrefix=HbaseRowkeyPrefix+"_"
  config.HbaseInputColName=HbaseInputColName
def SaveDataframeToHbase(): Unit ={
  val conf = HBaseConfiguration.create()
  //设置zooKeeper集群地址，也可以通过将hbase-site.xml导入classpath，但是建议在程序里这样设置
  conf.set(config.zooKeeper_quorum,config.zooKeeper_quorum_value)
  //设置zookeeper连接端口，默认2181
  conf.set(config.zooKeeper_property_clientPort, config.zooKeeper_property_clientPort_value)

  val jobConf = new JobConf(conf)
  jobConf.setOutputFormat(classOf[TableOutputFormat])
  jobConf.set(TableOutputFormat.OUTPUT_TABLE, config.HbaseTableName)
  val rdd = dataframe.toJavaRDD.rdd.map{Row=>{
    /*一个Put对象就是一行记录，在构造方法中指定主键
     * 所有插入的数据必须用org.apache.hadoop.hbase.util.Bytes.toBytes方法转换
     * Put.add方法接收三个参数：列族，列名，数据
     */
    val put = new Put(Bytes.toBytes(config.HbaseRowkeyPrefix+Row(1)))
    put.add(Bytes.toBytes("index"),Bytes.toBytes(config.HbaseInputColName),Bytes.toBytes(Row(0)+""))
    //转化成RDD[(ImmutableBytesWritable,Put)]类型才能调用saveAsHadoopDataset
    (new ImmutableBytesWritable, put)
  }}

  rdd.saveAsHadoopDataset(jobConf)
}

}
