package com.harmonycloud.readdata

import com.harmonycloud.config.Config
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.spark.sql.{DataFrame, SQLContext}
import org.apache.spark.{SparkConf, SparkContext}

class ReadDataFromMysql (mysqlIP:String,mysqlPort:String,dataBaseName:String,
                         dataTableName:String,user:String,password:String,
                         keyColName:String,timeStampColName:String,timeCeilFrom:String,
                         timeFloorTo:String,sparkConf:SparkConf){
  //mysql 传递参数1：IP  2:Port  3.数据库名称 4.数据库表名 5.账号  6.密码  7.关键字段列名
  //8.时间戳列名  9.时间戳上界  10.时间戳下界 11.Sparkconf
  var config = new Config()
  config.source = "jdbc"
  config.driver = "com.mysql.cj.jdbc.Driver"
  config.sourceIP = mysqlIP
  config.sourcePort = mysqlPort
  config.sourceDataPath = dataBaseName
  config.dataTableName = dataTableName
  config.loginUserName = user
  config.loginPassword = password
  config.keyColName = keyColName
  config.timeStampColName = timeStampColName
  config.timeCeilFrom = timeCeilFrom
  config.timeFloorTo = timeFloorTo
  def readDataToDataFrame():DataFrame = {
    //sparkConf.set("spark.driver.extraClassPath", "/home/filetemp/hbase-1.2.6.1/conf")
    val sc = new SparkContext(sparkConf)
    val sqlContext = new SQLContext(sc)
    val url = "jdbc:mysql://" + config.sourceIP  + ":" + config.sourcePort + "/" + config.sourceDataPath
    val sql = "select * from " + config.dataTableName
    var wheresql="select * from temp where "+ config.timeStampColName +
      " between '" + config.timeCeilFrom + "' and '" + config.timeFloorTo + "'"//根据时间戳上下界筛选数据
    /*
      * spark自定义sql语句获取数据
      *
      *
      * */
    val datafromMysql = sqlContext.read.format(config.source).options(Map("url"->url,
      "driver" ->config.driver ,
      "dbtable"-> config.dataTableName ,
      "user"->config.loginUserName,"password"->config.loginPassword)).load()
    datafromMysql.registerTempTable("temp")
    datafromMysql.sqlContext.sql(wheresql).collect.foreach(println)
    var dfselect=datafromMysql.sqlContext.sql(wheresql)
    return dfselect
  }
}
