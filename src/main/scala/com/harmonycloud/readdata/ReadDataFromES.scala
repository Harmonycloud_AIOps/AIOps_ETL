package com.harmonycloud.readdata
import org.elasticsearch.spark.sql
import com.harmonycloud.config.Config
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.{DataFrame, SQLContext}

class ReadDataFromES (esIP:String,esPort:String,IndexName:String,keyColName:String,
                      timeStampColName:String,timeCeilFrom:String,timeFloorTo:String,sparkConf: SparkConf){
  //传递参数：1.esNodeIP  2.esPort 3.索引名 4.关键字段 5.时间戳列名 6.筛选时间戳上界 7.筛选时间戳下界 8.Sparkconf
  var config = new Config()
  config.source = "org.elasticsearch.spark.sql"
  config.sourceIP = esIP
  config.sourcePort = esPort
  config.sourceDataPath = IndexName
  config.keyColName = keyColName
  config.timeStampColName = timeStampColName
  config.timeCeilFrom = timeCeilFrom
  config.timeFloorTo = timeFloorTo



  def readDataToDataFrame():DataFrame= {
    sparkConf.set("es.mapping.date.rich", "false") //否则读取es中的自定义格式日期时无法筛选
    val sc = new SparkContext(sparkConf)
    val sqlCtx = new SQLContext(sc)
    val options = Map("pushdown" -> "true","es.nodes" ->config.sourceIP, "es.port" -> config.sourcePort)
    val sparkDF = sqlCtx.read.format(config.source).options(options).load(config.sourceDataPath)
    sparkDF.registerTempTable("user")
    //var sql="select fs.used_p,indexTime from user where indexTime between '2018-07-19T00:00:43' and '2018-07-19T00:10:13'"
    var sql="select "+keyColName+","+timeStampColName+" from user where "+timeStampColName+" between '"+timeCeilFrom+"' and '"+timeFloorTo+"'"
    var userTable = sqlCtx.sql(sql)
    //从这里增加筛选条件而不是到下面的transDF可以完成对时间戳关键字段的筛选
    var transDF = userTable.toDF("user_p","timestamp")
    transDF.show(100)
    return transDF
  }
}
