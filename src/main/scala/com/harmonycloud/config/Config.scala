package com.harmonycloud.config

class Config extends Serializable {
  /*
  * 以下两个参数用来设置master节点和APPName
  *
  var masterSet:String=""
  var APPName:String=""
  */
  /*
  * 以下四个参数主要针对sqlContest.read.format(源数据读取)函数的相关配置
  * 其中source表示使用的数据库连接类型(jdbc/org.elasticsearch.spark.sql)
  *数据库驱动名
  * */
  var source:String=""
  var driver:String=""
  //var options:Map[Nothing, Nothing]=Map()
  var loginUserName:String=""
  var loginPassword:String=""
  /*
  *
  * 以下三个参数主要对接
  * 源数据IP，
  * Port以及
  * sourceDataPath:数据库名称(mysql)/esindex(elasticSearch)
  *
  * */
  var sourceIP:String=""
  var sourcePort:String=""
  var sourceDataPath:String=""

  /*
  * 以下参数主要对接数据表名（es不需要）
  * 事件戳对应列名
  * 时间戳上界
  * 时间戳下界
  *
  * */
  var dataTableName:String=""
  var keyColName:String=""
  var timeStampColName:String=""
  var timeCeilFrom:String=""
  var timeFloorTo:String=""

  /*
  *
  * 以下针对zookeeper参数配置
  *
  */
  var zooKeeper_quorum_value:String="slave1,slave2,slave3"
  var zooKeeper_quorum:String="hbase.zookeeper.quorum"
  var zooKeeper_property_clientPort:String="hbase.zookeeper.property.clientPort"
  var zooKeeper_property_clientPort_value:String="2181"


  /*
  * 以下针对Hbase插入数据
  *HbaseTableName:插入的Hbase table name
  * HbaseRowkeyPrefix:插入的数据Rowkey前缀 Rowkey=HbaseRowkeyPrefix+时间戳
  */
  var HbaseTableName:String=""
  var HbaseRowkeyPrefix:String=""
  var HbaseInputColName:String=""

}
